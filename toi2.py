# toi2.py
# Prediction of values in time series and anomaly detection, TOI project 2, 2.5.2022
# Author: Matej Kudera, xkuder04, VUT FIT
# Main file for time series processing

######################## Imports #########################
from cProfile import label
from cmath import nan
import datetime

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from tensorflow.keras.models import Sequential
import tensorflow.keras.layers as layers


from statsmodels.tsa.holtwinters import ExponentialSmoothing
from scipy.stats import norm

from sklearn.metrics import mean_absolute_error,mean_squared_error

######################## Functions #######################
# Add delta tu datetime
def get_line_datetime(delta_seconds, time):
    return time + datetime.timedelta(seconds=delta_seconds)

# Load dataframe from .csv file and change it do needed format
def load_data():
    # Load csv with Golden gate bridge accelerometer data
    bridge_df = pd.read_csv('Golden Gate Bridge Accelerometer Data.csv')  

    # Change first column to datetime with applied delta
    actual_datetime = datetime.datetime.now()
    bridge_df['time'] =  bridge_df.apply(lambda x: get_line_datetime(x['time'], actual_datetime), axis=1)

    # Set datetime as index of dataframe
    bridge_df = bridge_df.set_index('time')

    # Resample dataframe with mean function in 0.2 second intervals
    bridge_df = bridge_df.resample('0.2S').mean()

    return bridge_df

# Create sequences for ax column with given number of input and output values
def create_x_sequences(bridge_df, input_sequence_size, output_sequence_size):
    input_lstm = []
    output_lstm = []
    for x in range(input_sequence_size + output_sequence_size - 1, bridge_df.shape[0]):
        one_input = []
        for i in range(x - (input_sequence_size + output_sequence_size - 1), x - output_sequence_size + 1):
            sub_df = bridge_df.iloc[[i]]
            one_input.append([sub_df.iloc[0]['ax']])

        input_lstm.append(one_input)

        one_output = []
        for i in range(x - output_sequence_size + 1, x+1):
            sub_df = bridge_df.iloc[[i]]
            one_output.append(sub_df.iloc[0]['ax'])

        output_lstm.append(one_output)

    return input_lstm, output_lstm

# Create sequences for ax,ay,az columns with given number of input and output values
def create_xyz_sequences(bridge_df, input_sequence_size, output_sequence_size):
    input_lstm = []
    output_lstm = []

    for x in range(input_sequence_size + output_sequence_size - 1, bridge_df.shape[0]):
        one_input = []
        for i in range(x - (input_sequence_size + output_sequence_size - 1), x - output_sequence_size + 1):
            sub_df = bridge_df.iloc[[i]]
            one_input.append([sub_df.iloc[0]['ax'], sub_df.iloc[0]['ay'], sub_df.iloc[0]['az']])

        input_lstm.append(one_input)

        one_output = []
        for i in range(x - output_sequence_size + 1, x+1):
            sub_df = bridge_df.iloc[[i]]
            one_output.append(sub_df.iloc[0]['ax'])

        output_lstm.append(one_output)

    return input_lstm, output_lstm


# Make and train LSTM network from x values
def make_LSTM_for_x(input_sequence_size, output_sequence_size, train_input, train_output, test_input, test_output):
    # Create NN
    model = Sequential()
    model.add(layers.LSTM(50,input_shape=(input_sequence_size,1),return_sequences=True))
    model.add(layers.LSTM(50,return_sequences=False))
    model.add(layers.Dense(20,kernel_initializer='normal',activation='linear'))
    model.add(layers.Dense(units=output_sequence_size, kernel_initializer='normal',activation='linear'))

    model.summary()
    model.compile(loss='mean_squared_error', optimizer='adam')

    # Fit NN
    model.fit(train_input, train_output, batch_size=1, epochs=20, verbose=1, validation_data=(test_input, test_output))

    return model

# Make and train LSTM network from x,y,z values
def make_LSTM_for_xyz(input_sequence_size, output_sequence_size, train_input, train_output, test_input, test_output):
    # Create NN
    model = Sequential()
    model.add(layers.LSTM(50,input_shape=(input_sequence_size,3),return_sequences=True))
    model.add(layers.LSTM(50,return_sequences=False))
    model.add(layers.Dense(20,kernel_initializer='normal',activation='linear'))
    model.add(layers.Dense(units=output_sequence_size, kernel_initializer='normal',activation='linear'))

    model.summary()
    model.compile(loss='mean_squared_error', optimizer='adam')

    # Fit NN
    model.fit(train_input, train_output, batch_size=1, epochs=20, verbose=1, validation_data=(test_input, test_output))

    return model

# Convert predicted sequences to one output
def condence_sequences(predicted_sequences):
    # Shift predicted sequences to their correct positions in output sequence
    shifted_sequences = []
    for x in range(0, len(predicted_sequences)):
        # Shift by start position
        sequence = [nan] * x

        # Insert sequence
        sequence.extend(predicted_sequences[x])

        shifted_sequences.append(sequence)

    # Make dataframe and calculate mean values
    sequences_pd = pd.DataFrame(shifted_sequences)
    sequences_pd = sequences_pd.mean()
    
    # Return result in list
    return sequences_pd.values.tolist()


####################### Main #############################
def main():
    ################# Prepare data ###################
    # Get dataframe with bridge data
    bridge_df = load_data()
    #print(bridge_df.head())

    # Create sequences for given parameters
    input_sequence_size = 20
    output_sequence_size = 6
        
    all_x_inputs, all_x_outputs = create_x_sequences(bridge_df, input_sequence_size, output_sequence_size)
    all_xyz_inputs, all_xyz_outputs = create_xyz_sequences(bridge_df, input_sequence_size, output_sequence_size)

    # Convert sequences to numpy arrays
    all_x_inputs = np.array(all_x_inputs)
    all_x_outputs = np.array(all_x_outputs)

    all_xyz_inputs = np.array(all_xyz_inputs)
    all_xyz_outputs = np.array(all_xyz_outputs)

    # Select 80% of values for training and use remaining 20% for testing
    train_input_x = all_x_inputs[0 : int(len(all_x_inputs) * 0.8)]
    train_output_x = all_x_outputs[0 : int(len(all_x_outputs) * 0.8)]
    test_input_x = all_x_inputs[int(len(all_x_inputs) * 0.8):]
    test_output_x = all_x_outputs[int(len(all_x_outputs) * 0.8):]

    train_input_xyz = all_xyz_inputs[0 : int(len(all_xyz_inputs) * 0.8)]
    train_output_xyz = all_xyz_outputs[0 : int(len(all_xyz_outputs) * 0.8)]
    test_input_xyz = all_xyz_inputs[int(len(all_xyz_inputs) * 0.8):]
    test_output_xyz = all_xyz_outputs[int(len(all_xyz_outputs) * 0.8):]

    # List of all values from ax column
    ax_list = bridge_df['ax'].tolist()

    ################### Use LSTM for ax values ###################
    model_x = make_LSTM_for_x(input_sequence_size, output_sequence_size, train_input_x, train_output_x, test_input_x, test_output_x)

    # Predict test and train values
    train_x_predict = model_x.predict(train_input_x) 
    test_x_predict = model_x.predict(test_input_x)

    # Get predicted values in one sequence
    train_x_predict_condenced = [nan] *input_sequence_size
    train_x_predict_condenced.extend(condence_sequences(train_x_predict))
    test_x_predict_condenced = [nan] * (len(train_x_predict_condenced))
    test_x_predict_condenced.extend(condence_sequences(test_x_predict)[output_sequence_size-1:]) # Items on borderline are allready used in train part

    # Plot values
    plt.figure()
    plt.title('Predictions using LSTM from column ax')
    plt.plot(ax_list, label='Original values')
    plt.plot(train_x_predict_condenced, label='Predicted train values')
    plt.plot(test_x_predict_condenced, label='Predicted test values')
    plt.legend()
    plt.xlabel('Sample')
    plt.ylabel('Ax value')
    plt.savefig("LSTM_ax.png")
    plt.show()

    # Get errors
    print("Prediction errors using LSTM from column ax:")
    print("Train data:")
    print(f'Mean Absolute Error = {mean_absolute_error(ax_list[input_sequence_size:len(train_x_predict_condenced) - output_sequence_size],train_x_predict_condenced[input_sequence_size:len(train_x_predict_condenced) - output_sequence_size])}')
    print(f'Mean Squared Error = {mean_squared_error(ax_list[input_sequence_size:len(train_x_predict_condenced) - output_sequence_size],train_x_predict_condenced[input_sequence_size:len(train_x_predict_condenced) - output_sequence_size])}')
    print("")
    print("Test data:")
    print(f'Mean Absolute Error = {mean_absolute_error(ax_list[len(train_x_predict_condenced):],test_x_predict_condenced[len(train_x_predict_condenced):])}')
    print(f'Mean Squared Error = {mean_squared_error(ax_list[len(train_x_predict_condenced):],test_x_predict_condenced[len(train_x_predict_condenced):])}')

    ################### Use LSTM for all values ##################
    model_xyz = make_LSTM_for_xyz(input_sequence_size, output_sequence_size, train_input_xyz, train_output_xyz, test_input_xyz, test_output_xyz)

    # Predict test and train values
    train_xyz_predict = model_xyz.predict(train_input_xyz) 
    test_xyz_predict = model_xyz.predict(test_input_xyz)

    # Get predicted values in one sequence
    train_xyz_predict_condenced = [nan] *input_sequence_size
    train_xyz_predict_condenced.extend(condence_sequences(train_xyz_predict))
    test_xyz_predict_condenced = [nan] * (len(train_xyz_predict_condenced))
    test_xyz_predict_condenced.extend(condence_sequences(test_xyz_predict)[output_sequence_size-1:]) # Items on borderline are allready used in train part

    # Plot values
    plt.figure()
    plt.title('Predictions using LSTM from columns ax,ay,az')
    plt.plot(ax_list, label='Original values')
    plt.plot(train_xyz_predict_condenced, label='Predicted train values')
    plt.plot(test_xyz_predict_condenced, label='Predicted test values')
    plt.legend()
    plt.xlabel('Sample')
    plt.ylabel('Ax value')
    plt.savefig("LSTM_ax,ay,az.png")
    plt.show()

    # Get errors
    print("Prediction errors using LSTM from columns ax,ay,az :")
    print("Train data:")
    print(f'Mean Absolute Error = {mean_absolute_error(ax_list[input_sequence_size:len(train_xyz_predict_condenced) - output_sequence_size],train_xyz_predict_condenced[input_sequence_size:len(train_xyz_predict_condenced) - output_sequence_size])}')
    print(f'Mean Squared Error = {mean_squared_error(ax_list[input_sequence_size:len(train_xyz_predict_condenced) - output_sequence_size],train_xyz_predict_condenced[input_sequence_size:len(train_xyz_predict_condenced) - output_sequence_size])}')
    print("")
    print("Test data:")
    print(f'Mean Absolute Error = {mean_absolute_error(ax_list[len(train_xyz_predict_condenced):],test_xyz_predict_condenced[len(train_xyz_predict_condenced):])}')
    print(f'Mean Squared Error = {mean_squared_error(ax_list[len(train_xyz_predict_condenced):],test_xyz_predict_condenced[len(train_xyz_predict_condenced):])}')

    ############## Show x and xyz LSTM prediction together ##################
    plt.figure()
    plt.title('Predictions using LSTM')
    plt.plot(ax_list, label='Original values')
    plt.plot(train_x_predict_condenced, label='Predicted train values from ax')
    plt.plot(test_x_predict_condenced, label='Predicted test values from ax')
    plt.plot(train_xyz_predict_condenced, label='Predicted train values from ax,ay,az')
    plt.plot(test_xyz_predict_condenced, label='Predicted test values from ax,az,az')
    plt.legend()
    plt.xlabel('Sample')
    plt.ylabel('Ax value')
    plt.savefig("LSTM_all.png")
    plt.show()

    ############### Make predictions on ax using Triple exponentional smoothing (Holt-Winters) ###############
    # Get 80% of ax values for training in dataframe
    ax_df = bridge_df['ax'][0:int(len(ax_list)*0.8)]
    ax_df.index.freq = '0.2S'

    # Fit Holt-Winters on train data
    hw_model = ExponentialSmoothing(ax_df,trend='add',seasonal='add', seasonal_periods=input_sequence_size).fit()

    # Predict train and test data
    train_x_fitted_HW = hw_model.fittedvalues.tolist()
    test_x_predict_HW = [nan] * len(train_x_fitted_HW)
    test_x_predict_HW.extend(hw_model.forecast(len(ax_list) - (int(len(ax_list)*0.8))).tolist())

    # Plot values
    plt.figure()
    plt.title('Predictions using Holt-Winters')
    plt.plot(ax_list, label='Original values')
    plt.plot(train_x_fitted_HW, label='Fitted train values')
    plt.plot(test_x_predict_HW, label='Predicted test values')
    plt.legend()
    plt.xlabel('Sample')
    plt.ylabel('Ax value')
    plt.savefig("HW.png")
    plt.show()

    # Get errors
    print("Prediction errors using Holt-Winters")
    print("Train data:")
    print(f'Mean Absolute Error = {mean_absolute_error(ax_list[0:len(train_x_fitted_HW)],train_x_fitted_HW)}')
    print(f'Mean Squared Error = {mean_squared_error(ax_list[0:len(train_x_fitted_HW)],train_x_fitted_HW)}')
    print("")
    print("Test data:")
    print(f'Mean Absolute Error = {mean_absolute_error(ax_list[len(train_x_fitted_HW):],test_x_predict_HW[len(train_x_fitted_HW):])}')
    print(f'Mean Squared Error = {mean_squared_error(ax_list[len(train_x_fitted_HW):],test_x_predict_HW[len(train_x_fitted_HW):])}')

    # Plot Holt-Winters with xyz LSTM
    plt.figure()
    plt.title('Predictions using Holt-Winters and LSTM from columns ax,ay,az')
    plt.plot(ax_list, label='Original values')
    plt.plot(train_x_fitted_HW, label='Fitted train values using HW')
    plt.plot(test_x_predict_HW, label='Predicted test values using HW')
    plt.plot(train_xyz_predict_condenced, label='Predicted train values using LSTM')
    plt.plot(test_xyz_predict_condenced, label='Predicted test values using LSTM')
    plt.legend()
    plt.xlabel('Sample')
    plt.ylabel('Ax value')
    plt.savefig("LSTM_vs_HW.png")
    plt.show()

    # Visualize confidence intervals and anomalies
    # Make dataframes from input and predicted values
    ax_predict = []
    ax_predict.extend(train_x_fitted_HW)
    ax_predict.extend(test_x_predict_HW[len(train_x_fitted_HW):])
    ax_predict_df = pd.DataFrame(ax_predict)
    ax_df = pd.DataFrame(ax_list)

    # Calculate error parameters prom input and predicted values
    ax_error_df = ax_predict_df - ax_df
    ax_mean = ax_error_df.mean()
    ax_std = ax_error_df.std()
    
    # Get y values of confidence intervals
    limit_high = norm.ppf(0.99, ax_mean, ax_std) + ax_predict_df
    limit_low = norm.ppf(0.01, ax_mean, ax_std) + ax_predict_df
    limit_high = limit_high[0].tolist()
    limit_low = limit_low[0].tolist()

    # Plot confidence intervals in Holt-Winters prediction
    plt.figure()
    plt.title('Confidence interval for Holt-Winters')
    plt.plot(ax_list, label='Original values')
    plt.plot(ax_predict, label='HW values')
    plt.fill_between(range(0, len(ax_list)), limit_high, limit_low, color='green', alpha=0.2, label='Confidence interval')
    plt.legend()
    plt.xlabel('Sample')
    plt.ylabel('Ax value')
    plt.savefig("HW_confidence_interval.png")
    plt.show()


if __name__ == "__main__":
    main()

# END toi2.py